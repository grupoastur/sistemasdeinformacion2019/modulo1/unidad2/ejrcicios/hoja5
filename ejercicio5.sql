﻿USE ciclistas;

-- 1 nombre y edad de los ciclistas que no han ganado etapas
      SELECT DISTINCT c.nombre, c.edad FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL;
  
-- 2 nombre y edad de los ciclistas que no han ganado puertos
     SELECT DISTINCT c.nombre, c.edad FROM ciclista c LEFT JOIN puerto p ON c.dorsal = p.dorsal WHERE p.dorsal IS NULL;
  
-- 3 listar el director de los equipos que tengan ciclistas que no hayan ganado ninguna etapa
     SELECT DISTINCT director FROM equipo JOIN (ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal)
     ON equipo.nomequipo= c.nomequipo WHERE e.dorsal IS NULL;
  
-- 4 dorsal y nombre de los ciclistas que no hayan llevado algún maillot
     SELECT c.dorsal, c.nombre, l.código FROM ciclista c LEFT JOIN lleva l ON c.dorsal = l.dorsal WHERE l.dorsal IS NULL; 

-- 5 dorsal y nombre de los ciclistas que no hayan llevado el maillot amarillo nunca
     SELECT l.dorsal FROM lleva l JOIN maillot m ON l.código = m.código WHERE m.color='amarillo';   
     SELECT c.dorsal, c.nombre FROM ciclista c LEFT JOIN(SELECT l.dorsal FROM lleva l JOIN maillot m ON l.código = m.código 
     WHERE m.color='amarillo') c1 ON c.dorsal= c1.dorsal WHERE c1.dorsal IS NULL; 

-- 6 indicar el numetapa de las etapas que no tengan puertos
     SELECT e.numetapa FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;


-- 7 indicar la distancia media de las etapas que no tengan puertos
     SELECT AVG( e.kms), e.numetapa, p.nompuerto FROM etapa e LEFT JOIN puerto p 
     ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;

-- 8 listar el número de ciclistas que no hayan ganado alguna etapa
     SELECT COUNT(*) FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal WHERE e.dorsal IS NULL; 

-- 9 listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tengan puerto
     SELECT DISTINCT c.dorsal FROM (ciclista c JOIN etapa e ON c.dorsal = e.dorsal )
     LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL ; 
  
-- 10 listar el dorsal de los ciclistas que hayan ganado únicamente etapas que no tengan puertos
      SELECT DISTINCT e.dorsal FROM etapa e LEFT JOIN puerto p ON e.numetapa = p.numetapa WHERE p.numetapa IS NULL;                                     
